<!DOCTYPE html>
<html>
  <!-- Header css meta -->
  @include('Layout.header', ['type' => 'home', 'title' => 'Register Account', 'icon' => asset('img/logo.png') ])
<body class="hold-transition login-page">
<div class="login-box ">
  <div class="login-logo">
    {{-- <img src="{{ asset('img/logo.png') }}" alt=""> --}}
    <p><b>Register Account</b></p>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign up to create your account</p>
      <form class="needs-validation" id="register_form" action="{{ url('/register_account') }}" novalidate>

          <div class="input-group mb-3">
            <input type="email" name="firstname" id="firstname" class="form-control" placeholder="Firstname">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-info"></span>
              </div>
            </div>
            <div class="invalid-feedback" id="err_firstname"></div>
          </div>
          <div class="input-group mb-3">
            <input type="email" name="lastname" id="lastname" class="form-control" placeholder="Lastname">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-info"></span>
              </div>
            </div>
            <div class="invalid-feedback" id="err_lastname"></div>
          </div>
          <div class="input-group mb-3">
            <input type="email" name="middlename" id="middlename" class="form-control" placeholder="Middlename">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-info"></span>
              </div>
            </div>
            <div class="invalid-feedback" id="err_middlename"></div>
          </div>

        <div class="input-group mb-3">
          <input type="email" name="email_address" id="email_address" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          <div class="invalid-feedback" id="err_email_address"></div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" id="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          <div class="invalid-feedback" id="err_password"></div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <a href="{{url('/')}}" class="btn btn-primary btn-block btn-sm">Login</a>
          </div>
          <div class="col-md-6">
            <button type="submit" id="btn_register" class="btn btn-primary btn-block btn-sm">Register</button>
          </div>
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
</body>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'home'])
</html>

<script type="text/javascript">
  $("#register_form").on('submit', function(e){
      
    var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          $('#btn_register').prop('disabled',true);
          $('#btn_register').text("Creating account...");
      },
      success:function(response){
          console.log(response);
        if(response.status == true){
          // console.log(response);
          window.location = main_path + '/';
          // swal("Success", response.message, "success");
          showValidator(response.error,'register_form');
        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'register_form');
        }

        $('#btn_register').prop('disabled',false);
        $('#btn_register').text("Sign Up");

      },
      error:function(error){
        console.log(error)
      }
    });
  });
</script>