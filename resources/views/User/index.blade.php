<!DOCTYPE html>

<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'user', 'title' => 'User', 'icon' => asset('img/logo.png') ])

<body class="sidebar-mini layout-fixed">
  <style>
    .display_none{
      display: none;
    }
  </style>
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'user'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'user'])
   <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
         
          @if(Auth::user()->security_options == 0)

          <div class="row" id="security">
            <div class="col-12 mt-3">
              <div class="card">
                <div class="card-header h4">
                  <i class="fas fa-lock fa-lg"></i> Security Options 
                  <h1 id="clock" class="text-center float-right"></h1>
                </div>
                <div class="card-body">
                  <div class="alert alert-primary alert-dismissible h3">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Reminder: </strong> Please select 3 security options to reset your account.
                  </div>
                  <form  class="needs-validation" id="add_form_security" action="{{ url('user/add_security_option') }}" novalidate>
                    <input type="hidden" class="form-control" name="email_address" id="email_address" value="{{Auth::user()->email_address}}"/>
                    <div class="form-group col-sm-12">
                      <label>Security Questions </label>
                      <select class="form-control" id="question" name="question" required="">
                        <option value="What primary school did you attend?">What primary school did you attend?</option>
                        <option value="In what town or city did you meet your spouse or partner?">In what town or city did you meet your spouse or partner?</option>
                        <option value="What is the name of your first pet?">What is the name of your first pet?</option>
                        <option value="In what town or city did your parents meet?">In what town or city did your parents meet?</option>
                        <option value="What is your oldest cousin’s first name?">What is your oldest cousin’s first name?</option>
                        <option value="What was your favorite elementary school teacher’s name?">What was your favorite elementary school teacher’s name?</option>
                      </select>
                      <div class="invalid-feedback" id="err_question"></div>
                    </div>

                    <div class="form-group col-sm-12">
                      <label>Answer </label>
                      <input type="text" id="answer" name="answer" placeholder="Answer" class="form-control " required>
                      <div class="invalid-feedback" id="err_answer"></div>
                    </div>

                    <div class="col-sm-12">
                      <button class="btn btn-success btn-sm" id="btn_save" type="submit">Save</button>
                      <span class="float-right" id="q_counter"></span>
                      <br>
                    </div>
                  </form>  
                </div>
                <div class="card-footer"></div>

              </div>
            </div>
          </div>
          @else
            {{-- WITH SECURITY OPTIONS --}}
          <div class="row" id="attendance">
            <div class="col-12 mt-3">
              <div class="card">
                <div class="card-header h4">
                  <i class="fas fa-clock fa-lg"></i> Attendance 
                  <h1 id="clock" class="text-center float-right"></h1>
                </div>
                <div class="card-body" id="load_data">
                  <div class="alert alert-primary alert-dismissible h3">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Schedule: </strong> 08:00 AM to 12:00 PM | 01:00 PM to 05:00 PM
                  </div>
                  <button class="btn btn-lg btn-success btn-block btn-log" style="display:none;" id="time_in_f" onclick="time_log('time_in_f');"> <span class="fas fa-sign-in-alt fa-lg "></span> Time In A.M</button>
                  <button class="btn btn-lg btn-danger btn-block btn-log" style="display:none;" id="time_out_f" onclick="time_log('time_out_f');"> <span class="fas fa-sign-out-alt fa-lg "></span> Time Out A.M.</button>
                  <button class="btn btn-lg btn-success btn-block btn-log" style="display:none;" id="time_in_l" onclick="time_log('time_in_l');"> <span class="fas fa-sign-in-alt fa-lg "></span> Time In P.M. </button>
                  <button class="btn btn-lg btn-danger btn-block btn-log" style="display:none;" id="time_out_l" onclick="time_log('time_out_l');"> <span class="fas fa-sign-out-alt fa-lg "></span> Time Out P.M.</button>
                  <button class="btn btn-lg btn-warning btn-block btn-log" style="display:none;" id="no_time_log" > <span class="fas fa-sign-out-alt fa-lg "></span> Come back at 08:00 AM</button>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>
            <div class="col-12 mt-3">
              <div class="card">
                <div class="card-header h4"><i class="fa fa-list-alt"></i> Attendance Logs for {{date('Y-m-d')}}</div>
                <div class="card-body">
                  <table class="table table-bordered dt-responsive nowrap" id="tbl_logs" style="width: 100%;"></table>
                </div>
              </div>
            </div>
          </div>
          {{-- WITH SECURITY OPTIONS --}}
          @endif
        </div>
      </section>
    </div>
  </div>
</body>
  <!-- Footer Scripts -->
@include('Layout.footer', ['type' => 'user'])
</html>
<script>
  show_logs();
  function time_log(type){
      var type = type;
      $.ajax({
        type:"POST",
        url:'{{url("user/time_log")}}',
        data:{type:type},
        cache:false,
        success:function(response){
          if(response.status == true){
            console.log(response)
            swal("Success", response.message, "success");
            show_logs();
          }else if(response.status == false){
            console.log(response)
            swal("Info", response.message, "info");
          }else{
            //<!-- your error message or action here! -->
            showValidator(response.error,'type');
          }
        },
        error:function(error){
          console.log(error)
        }
      });
  }
  
  function time_log_button(){
 
    $.ajax({
      type:"POST",
      url:'{{url("user/time_log_button")}}',
      cache:false,
      success:function(response){
        //alert('#'+response);
        $('#'+response).removeClass("btn-log");
        $('#'+response).css('display','block');
        
        $('.btn-log').css('display','none');
        
      },
      error:function(error){
        console.log(error)
      }
    });

  }
  
  time_log_button();
  var myVar = setInterval(myTimer, 1000);

  function myTimer() {
    //time_log_button();
    var d = new Date();
    document.getElementById("clock").innerHTML = d.toLocaleTimeString();
  }

  var tbl_logs;
  function show_logs(){
    if (tbl_logs) {
      tbl_logs.destroy();
    }
    var url = main_path + '/user/load_logs_today';
    tbl_logs = $('#tbl_logs').DataTable({
    pageLength: 10,
    responsive: true,
    bFilter: false,
    bInfo: false,
    lengthChange:false,
    bPaginate:false,
    ajax: url,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
    },
    columns: [{
      className: '',
      "data": "time_in_f",
      "title": "Time In A.M.",
    },{
      className: '',
      "data": "time_out_f",
      "title": "Time Out A.M.",
    }
    ,{
      className: '',
      "data": "time_in_l",
      "title": "Time In P.M.",
    }
    ,{
      className: '',
      "data": "time_out_l",
      "title": "Time Out P.M",
    },{
      className: '',
      "data": "date_trans",
      "title": "Date",
    }
    ]
    });
  }

  // Add Security Question
  $("#add_form_security").on('submit', function(e){
    var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          $('#btn_save').prop('disabled',true);
          $('#btn_save').text('Saving...');
      },
      success:function(response){

        if(response.status == '2'){
          swal("Success", response.message, "success");
          showValidator(response.error,'add_form_security');
          $('#answer').val('');
          load_counter();
        }else if(response.status == '3'){
          swal("Information", response.message, "info");
          showValidator(response.error,'add_form_security');
        }else if(response.status == '1'){
          swal("Sucess", response.message, "success");
          location.reload();
        }
        $('#btn_save').prop('disabled',false);
        $('#btn_save').text('Save');
        
      },
      error:function(error){
        console.log(error)
      }
    });
  });
  load_counter();
  function load_counter(){
    
    $.ajax({
      type:"GET",
      url:'{{url("user/question_counter")}}',
      cache:false,
      success:function(response){
        $('#q_counter').html('Total Question: '+ response +' out of 3');
      },
      error:function(error){
        console.log(error)
      }
    });
  }
  
</script>
