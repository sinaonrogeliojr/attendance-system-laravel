<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Attendance;
use Illuminate\Support\Facades\DB;
use Validator;
class AdminController extends Controller
{
    //
    function index(){
        return view('Admin.index');
    }

    function list_employee(){
		$employee = User::where('user_type', 2)->whereNull('deleted_at')->get();
		return response()->json(['status' => true, 'data' => $employee]);
	}

    function add_employee(Request $request){

		$user_id= $request->get('user_id');
		$firstname = $request->get('firstname');
		$lastname = $request->get('lastname');
		$middlename = $request->get('middlename');
		$gender = $request->get('gender');
		$birthday = $request->get('birthday');
		$address = $request->get('address');
		$country = $request->get('country');
		$email_address = $request->get('email_address');
		$password = $request->get('password');
		$is_verified = $request->get('is_verified');
		
		$validator = Validator::make($request->all(), [
			'firstname' => 'required',
			'lastname' => 'required',
			'middlename' => 'required',
			'email_address' => 'required|email',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($user_id)) {
				$employee = User::find($user_id);
				$employee->firstname = $firstname;
				$employee->lastname = $lastname;
				$employee->middlename = $middlename;
				$employee->email_address = $email_address;
				$employee->gender = $gender;
				$employee->birthdate = $birthday;
				$employee->address = $address;
				$employee->country = $country;
				if(!empty($password)){
					$employee->password = $password;
				}
				$employee->is_verified = $is_verified;
				
				if($employee->save()){
					return response()->json(['status' => true, 'message' => 'Employee updated successfully!']);
				}
			}else{
				$employee = new User;
				$employee->firstname = $firstname;
				$employee->lastname = $lastname;
				$employee->middlename = $middlename;
				$employee->email_address = $email_address;
				$employee->gender = $gender;
				$employee->birthdate = $birthday;
				$employee->address = $address;
				$employee->country = $country;
				$employee->password = $password;
				$employee->is_verified = $is_verified;

				if($employee->save()){
					return response()->json(['status' => true, 'message' => 'Employee saved successfully!']);
				}
			}
		}
	}

	function delete_employee($user_id){
		$employee = User::find($user_id);
		if($employee->delete()){
			return response()->json(['status' => true, 'message' => 'Employee deleted successfully!']);
		}
	}

	function attendance(){
        return view('Admin.attendance');
    }

	function list_attendance(Request $request, $date_from, $date_to){
		//$attendance = Attendance::all();
		
		$attendance = DB::table('tbl_users')
		->join('tbl_attendance', 'tbl_users.user_id', '=', 'tbl_attendance.user_id')
		->select(DB::raw("CONCAT(tbl_users.firstname,' ',tbl_users.lastname) as full_name"),
		'tbl_attendance.time_in_f','tbl_attendance.time_out_f',
		'tbl_attendance.time_in_l','tbl_attendance.time_out_l',
		'tbl_attendance.date_trans','tbl_attendance.user_id')
		->where('tbl_attendance.date_trans', '>=',  $date_from)
		->where('tbl_attendance.date_trans', '<=',  $date_to)
		->get();
		return response()->json(['status' => true, 'data' => $attendance]);
	}

	public function time_format($time){
		
		return $time->format('h:i: A');

	}

}