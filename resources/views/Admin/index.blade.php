<!DOCTYPE html>

<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Admin', 'icon' => asset('img/logo.png') ])

<body class="sidebar-mini layout-fixed" onload="show_employee();">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'admin'])
   <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 mt-3">
              <div class="card">
                <div class="card-header h4">
                  <i class="fa fa-users"></i> Manage Employee Account
                  <button class="btn btn-primary float-right btn-sm" onclick="add_employee()"><i class="fa fa-plus"></i> Add Employee</button>
                </div>
                <div class="card-body">
                  <table class="table table-bordered dt-responsive nowrap" id="tbl_employee" style="width: 100%;"></table>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>

<div class="modal fade" role="dialog" id="modal_add_employee">
  <div class="modal-dialog modal-lg">
     <div class="modal-content">
        <div class="modal-header">
           <div class="modal-title">
              Add New User
           </div>
           <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
           <!--NAVIgation bar-->
           <form  class="needs-validation" id="add_form_employee" action="{{ url('admin/add_employee') }}" novalidate>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="page1" data-toggle="tab" href="#page1_tab" role="tab" aria-controls="page1_tab" aria-selected="true">Profile</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="page2" data-toggle="tab" href="#page2_tab" role="tab" aria-controls="page3_tab" aria-selected="false">Account</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active pt-2" id="page1_tab" role="tabpanel" aria-labelledby="page1">
                <input type="hidden" id="user_id" name="user_id" placeholder="" class="form-control" required>
                <input type="hidden" id="is_verified" value="1" name="is_verified" placeholder="" class="form-control" required>
                <div class="col-md-12">
                  <div class="row">
                    <div class="form-group col-sm-4">
                      <label>Firstname </label>
                      <input type="text" id="firstname" name="firstname" placeholder="Firstname" class="form-control " required>
                      <div class="invalid-feedback" id="err_firstname"></div>
                    </div>
                    <div class="form-group col-sm-4">
                      <label>Lastname </label>
                      <input type="text" id="lastname" name="lastname" placeholder="Lastname" class="form-control " required>
                      <div class="invalid-feedback" id="err_lastname"></div>
                    </div>
                    <div class="form-group col-sm-4">
                      <label>Middlename </label>
                      <input type="text" id="middlename" name="middlename" placeholder="Middlename" class="form-control " required>
                      <div class="invalid-feedback" id="err_middlename"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label>Gender </label>
                      <select class="form-control" id="gender" name="gender">
                        <option value="Female" selected="">Female</option>
                        <option value="Male">Male</option>
                      </select>
                      <div class="invalid-feedback" id="err_gender"></div>
                    </div>
                    <div class="form-group col-sm-6">
                      <label>Birthday </label>
                      <input type="date" id="birthday" value="{{date('Y-m-d')}}" name="birthday" placeholder="Birthday" class="form-control " required>
                      <div class="invalid-feedback" id="err_birthday"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="row">
                    <div class="form-group col-sm-6">
                      <label>Address </label>
                      <input type="text" id="address" name="address" placeholder="Address" class="form-control " required>
                      <div class="invalid-feedback" id="err_address"></div>
                    </div>
                    <div class="form-group col-sm-6">
                      <label>Country </label>
                      <input type="text" id="country" name="country" placeholder="Country" class="form-control " required>
                      <div class="invalid-feedback" id="err_country"></div>
                    </div>
                  </div>
                </div>
                
                <div class="form-group col-sm-12">
                  <label>Account Status </label>
                  <div class="custom-control custom-switch">
                    <input type="checkbox" checked="" onclick="verify_action();" class="custom-control-input" id="v_check" name="v_check">
                    <label class="custom-control-label" for="v_check">Account Verified</label>
                  </div>
                </div>
                <div class="col-sm-12 text-right">
                  <button class="btn btn-success btn-sm" onclick="next_tab('page2', 'page1');" type="button">Next</button>
                </div>
              </div>
              <div class="tab-pane fade pt-2" id="page2_tab" role="tabpanel" aria-labelledby="page2">
                <div class="form-row">
                  <div class="form-group col-sm-12">
                    <label>Email Address </label>
                    <input type="text" id="email_address" name="email_address" placeholder="Email Address" class="form-control " required>
                    <div class="invalid-feedback" id="err_email_address"></div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label>Password </label>
                    <input type="password" id="password" name="password" placeholder="Enter password" class="form-control ">
                    <div class="invalid-feedback" id="err_password"></div>
                  </div>
                  <div class="col-sm-12 text-right">
                    <button class="btn btn-dark btn-sm" type="button" onclick="next_tab('page2', 'page1', 'page3');">Previous</button>
                    <button class="btn btn-success btn-sm" type="submit">Save</button>
                  </div>
                </div>
              </div>
            </div>
           </form>
        </div>
        <div class="modal-footer">
        </div>
     </div>
  </div>
</div>

  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
</html>

<script type="text/javascript">

  function next_tab(show, hide1){
    $("#"+show).addClass('active');
    $("#"+hide1).removeClass('active');
    
    $("#"+show+'_tab').addClass('show active');
    $("#"+hide1+'_tab').removeClass('show active');
    
  }

  function add_employee(){
    $("#modal_add_employee").modal('show');
  }

</script>

<script>
  var tbl_employee;
  function show_employee(){
    if (tbl_employee) {
      tbl_employee.destroy();
    }
    var url = main_path + '/admin/list_employee';
    tbl_employee = $('#tbl_employee').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
    },
      columns: [{
      className: '',
      "data": "lastname",
      "title": "Lastname",
    },{
      className: '',
      "data": "firstname",
      "title": "Firstname",
    },{
      className: '',
      "data": "email_address",
      "title": "Email_address",
    },{ 
      title: "Status",
      data: 'is_verified', 
      render: function(data) { 
        if(data == 1) {
          return '<button class="btn btn-success btn-sm font-base"><span class="fa fa-check-circle fa-lg"></span> Verified</button>'
        }
        else {
          return '<button class="btn btn-warning btn-sm font-base"><span class="fa fa-exclamation-triangle fa-lg"></span> Not Verified</button>'
        }
      },
    },{
      className: 'width-option-1 text-center',
      "data": "user_id",
      "orderable": false,
      "title": "Options",
        "render": function(data, type, row, meta){
          var param_data = JSON.stringify(row);
          newdata = '';
          newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_employee(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
          newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_employee(this)" type="button"><i class="fa fa-edit"></i> Delete</button>';
          return newdata;
        }
      }
    ]
    });
  }

  $("#add_form_employee").on('submit', function(e){
    var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
          $('#btn_save').prop('disabled',true);
          $('#btn_save').text('Saving...');
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          // console.log(response)
          show_employee();
          swal("Success", response.message, "success");
          showValidator(response.error,'add_form_employee');
          $('#modal_add_employee').modal('hide');
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'add_form_employee');
        }

        $('#btn_save').prop('disabled',false);
        $('#btn_save').text('Save');

      },
      error:function(error){
        console.log(error)
      }
    });
  });

  function delete_employee(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    var url =  main_path + '/admin/delete_employee/' + data.user_id;
      swal({
        title: "Are you sure?",
        text: "Do you want to delete this employee?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          show_employee();
          swal("Success", response.message, "success");
        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
      });
    });
  }

  function edit_employee(_this){
    add_employee();
    var data = JSON.parse($(_this).attr('data-info'));
    $('#user_id').val(data.user_id);
    $('#firstname').val(data.firstname);
    $('#lastname').val(data.lastname);
    $('#middlename').val(data.middlename);
    $('#email_address').val(data.email_address);
    $('#is_verified').val(data.is_verified);
    if(data.is_verified == 1){
      $('#v_check').prop('checked', true);
    }else{
      $('#v_check').prop('checked', false);
    }
    
  }

  function verify_action() {
    var checkBox = document.getElementById("v_check");
    if (checkBox.checked == true){
      $('#is_verified').val(1);
    } else {
      $('#is_verified').val(0);
    }
  }

</script>
