<!DOCTYPE html>
<html>
  <!-- Header css meta -->
  @include('Layout.header', ['type' => 'home', 'title' => 'Recover Password', 'icon' => asset('img/logo.png') ])
<body class="hold-transition login-page">
<div class="login-box ">
  <div class="login-logo">
    {{-- <img src="{{ asset('img/logo.png') }}" alt=""> --}}
    <p><b>Recover Password</b></p>
  </div>

  {{-- Recover Form --}}

  <div class="card" id="recover_form">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Follow this steps to reset your password.</p>
      <ul class="nav nav-tabs mb-2" id="myTab" role="tablist" style="display: none;">
        <li class="nav-item">
          <a class="nav-link active" id="page1" data-toggle="tab" href="#page1_tab" role="tab" aria-controls="page1_tab" aria-selected="true">Email Check</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="page2" data-toggle="tab" href="#page2_tab" role="tab" aria-controls="page3_tab" aria-selected="false">Question 1</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="page3" data-toggle="tab" href="#page3_tab" role="tab" aria-controls="page4_tab" aria-selected="false">Change Password</a>
        </li>
      </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane active pt-2" id="page1_tab" role="tabpanel" aria-labelledby="page1">
                <form class="needs-validation" id="enter_email" action="{{ url('/get_email') }}" novalidate>
                    <div class="input-group mb-3">
                        <input type="email" required="" name="email_address" id="email_address" class="form-control" placeholder="Enter you email">
                        <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                        </div>
                        <div class="invalid-feedback" id="err_email_address"></div>
            
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        <button type="submit" id="btn_reset" class="btn btn-primary btn-block btn-sm">Next</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane pt-2" id="page2_tab" role="tabpanel" aria-labelledby="page2">
                <form class="needs-validation" id="submit_answer" action="{{ url('/submit_answer') }}" novalidate>

                    <label id="lbl_question"></label>

                    <div class="input-group mb-3">
                        <input type="text" name="answer" id="answer" class="form-control" placeholder="Answer">
                        <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-question"></span>
                        </div>
                        </div>
                        <div class="invalid-feedback" id="err_answer"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                        <button type="submit" id="btn_reset" class="btn btn-primary btn-block btn-sm">Next</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane pt-2" id="page3_tab" role="tabpanel" aria-labelledby="page3">
                <form class="needs-validation" id="enter_password" action="{{ url('/save_new_pass') }}"  novalidate>
                    <div class="form-group col-sm-12">
                        <label>New Password </label>
                        <input type="password" id="newpassword" name="newpassword" placeholder="New Password" class="form-control " required>
                        <div class="invalid-feedback" id="err_newpassword"></div>
                      </div>
                      <div class="form-group col-sm-12">
                        <label>Confirm Password </label>
                        <input type="password" id="password_confirmation" name="password_confirmation" placeholder="Confirm New Password" class="form-control " required>
                        <div class="invalid-feedback" id="err_password_confirmation"></div>
                      </div>
                    <div class="row">
                        <div class="col-md-12">
                        <button type="submit" id="btn_reset" class="btn btn-primary btn-block btn-sm">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>

</div>
    

</body>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'home'])
</html>

<script type="text/javascript">
  $("#enter_email").on('submit', function(e){
    var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(response){
        if(response.status == '1'){
            $('#page1').removeClass('active');
            $('#page3').removeClass('active');
            $('#page2').addClass('active');
            $('#page2_tab').addClass('active');
            $('#page1_tab').removeClass('active');
            $('#page3_tab').removeClass('active');
            load_question();
        }else if(response.status == '404'){
            showValidator(response.error,'enter_email');
        }else if(response.status == '0'){
            swal("Warning", response.message, "warning");
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });

  function load_question(){
    var email_address = $('#email_address').val();
    $.ajax({
      type:"POST",
      data:{email_address:email_address},
      url:'{{url("/load_question")}}',
      cache:false,
      success:function(response){
        if(response == '404'){
            swal("Error","Error on retrieving account.", "error");
            $('#email_address').val('');
            $('#page3').removeClass('active');
            $('#page1').addClass('active');
            $('#page2').removeClass('active');
            $('#page2_tab').removeClass('active');
            $('#page1_tab').addClass('active');
            $('#page3_tab').removeClass('active');
        }else{
            $('#lbl_question').html(response);
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  }

  $("#submit_answer").on('submit', function(e){
    var url = $(this).attr('action');

    var email_address = $('#email_address').val();
    var answer = $('#answer').val();
    var question = $('#lbl_question').text();

    var mydata = 'email_address=' + email_address + '&answer=' + answer + '&question=' + question;
    e.stopPropagation();
    e.preventDefault(e);
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(response){
        if(response.status == true){
            $('#page3').addClass('active');
            $('#page1').removeClass('active');
            $('#page2').removeClass('active');
            $('#page2_tab').removeClass('active');
            $('#page1_tab').removeClass('active');
            $('#page3_tab').addClass('active');
            swal("Success", response.message, "success");
        }else{
            swal("Warning", response.message, "warning");
        }
      },
      error:function(error){
        console.log(error)
      }
    
    });

  });

  $("#enter_password").on('submit', function(e){
      
    var url = $(this).attr('action');
    var email_address = $('#email_address').val();
    var newpassword = $('#newpassword').val();
    var password_confirmation = $('#password_confirmation').val();
    var mydata = 'email_address=' + email_address + '&newpassword=' + newpassword + '&password_confirmation=' + password_confirmation;
    e.stopPropagation();
    e.preventDefault(e);
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      success:function(response){
        if(response.status == true){
            console.log(response)
            swal("Success", response.message, "success");
            showValidator(response.error,'enter_password');

            window.location.href = "{{URL::to('/')}}"

        }else{
            //<!-- your error message or action here! -->
            showValidator(response.error,'enter_password');
        }
      },
      error:function(error){
        console.log(error)
      }
    
    });

  });

</script>