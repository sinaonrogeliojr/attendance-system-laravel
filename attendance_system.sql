/*
SQLyog Ultimate v12.2.6 (64 bit)
MySQL - 10.4.10-MariaDB : Database - attendance_system
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `tbl_attendance` */

DROP TABLE IF EXISTS `tbl_attendance`;

CREATE TABLE `tbl_attendance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `time_in_f` varchar(20) DEFAULT NULL,
  `time_out_f` varchar(20) DEFAULT NULL,
  `time_in_l` varchar(20) DEFAULT NULL,
  `time_out_l` varchar(20) DEFAULT NULL,
  `date_trans` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tbl_attendance` */

/*Table structure for table `tbl_user_option` */

DROP TABLE IF EXISTS `tbl_user_option`;

CREATE TABLE `tbl_user_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `email_address` varchar(200) DEFAULT NULL,
  `question` varchar(255) DEFAULT NULL,
  `answer` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user_option` */

/*Table structure for table `tbl_users` */

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `user_id` int(255) NOT NULL AUTO_INCREMENT,
  `profile_picture` longtext DEFAULT NULL,
  `firstname` varchar(100) DEFAULT '',
  `lastname` varchar(100) DEFAULT '',
  `middlename` varchar(100) DEFAULT '',
  `gender` varchar(7) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_type` int(1) DEFAULT 2 COMMENT '1 = admin, 2 = User',
  `is_verified` int(1) DEFAULT 0,
  `security_options` int(1) DEFAULT 0 COMMENT '0 = no, 1 = yes',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4;

/*Data for the table `tbl_users` */

insert  into `tbl_users`(`user_id`,`profile_picture`,`firstname`,`lastname`,`middlename`,`gender`,`birthdate`,`address`,`country`,`email_address`,`password`,`user_type`,`is_verified`,`security_options`,`created_at`,`updated_at`,`deleted_at`) values 
(39,NULL,'Juan','Dela Cruz','C','Male','1996-12-31',NULL,NULL,'admin@gmail.com','$2y$10$6vqzXlCFA3sZNkUrecwzLu3bXCYW0PBUuGhRFioAyC6FCYDtONPqC',1,1,0,'2021-03-22 12:04:20',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
