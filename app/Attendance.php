<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    //
    protected $primaryKey = 'id';

    protected $table = 'tbl_attendance';
    
}
