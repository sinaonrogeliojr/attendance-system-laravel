<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $hidden = [
        'password', 'remember_token',
    ];
    
    protected $primaryKey = 'user_id';

    protected $table = 'tbl_users';

    protected $fillable = [
        'profile_picture','firstname', 'lastname', 'middlename', 'password', 'email_address', 'gender', 'birthdate', 'address','country', 'user_type', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function getFullnameAttribute(){
        return $this->firstname.' '.$this->lastname;
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }

}
