<!DOCTYPE html>

<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'admin', 'title' => 'Admin', 'icon' => asset('img/logo.png') ])

<body class="sidebar-mini layout-fixed" onload="show_attendance();">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'admin'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'admin'])
   <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 mt-3">
              <div class="card">
                <div class="card-header h4">
                  <i class="fa fa-list-alt"></i> Attendance Logs
                  <div class="row col-md-12 mt-2">
                    <div class="form-check-inline col-4">
                      <small for="date_from"> DateFrom</small>
                      <input type="date" onchange="show_attendance();" value="{{date('Y-m-d')}}" id="date_from" name="date_from" class="form-control"/>
                    </div>
                    <div class="form-check-inline col-4">
                      <small for="date_to"> DateTo</small>
                      <input type="date" onchange="show_attendance();" value="{{date('Y-m-d')}}" id="date_to" name="date_to" class="form-control"/>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <table class="table table-bordered dt-responsive nowrap" id="tbl_attendance" style="width: 100%;"></table>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  </div>
</body>

  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'admin'])
</html>

<script>
  var tbl_attendance;
  function show_attendance(){

    var date_from = $('#date_from').val();
    var date_to = $('#date_to').val();

    if (tbl_attendance) {
        tbl_attendance.destroy();
    }
    var url = main_path + '/admin/list_attendance/' + date_from +'/' +date_to;

    tbl_attendance = $('#tbl_attendance').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
    },
      columns: [{
      className: '',
      "data": "user_id",
      "title": "UserId",
    },{
      className: '',
      "data": "full_name",
      "title": "Name",
    },{
      className: '',
      "data": "time_in_f",
      "title": "Time In A.M.",
    },{
      className: '',
      "data": "time_out_f",
      "title": "Time Out A.M.",
    }
    ,{
      className: '',
      "data": "time_in_l",
      "title": "Time In P.M.",
    }
    ,{
      className: '',
      "data": "time_out_l",
      "title": "Time Out P.M",
    },{
      className: '',
      "data": "date_trans",
      "title": "Date",
    }
    ]
    });
  }

</script>
