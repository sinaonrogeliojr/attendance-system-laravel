<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOption extends Model
{
    //
    protected $primaryKey = 'id';

    protected $table = 'tbl_user_option';
}
