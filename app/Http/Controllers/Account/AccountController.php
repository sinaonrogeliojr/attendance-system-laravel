<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;

use App\User;
use App\UserOption;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    //
    function login_user(Request $request){

    	$result = array();

    	$user_login = array(
    		'email_address' => $request->get('email_address'),
    		'password' => $request->get('password')
    	);

    		$validator = Validator::make($request->all(), [
              'email_address' => 'required',
              'password' => 'required',
        ]);

      if ($validator->fails()) {
        return response()->json(['status' => false, 'error' => $validator->errors()]);
      }else{
      
            if (Auth::attempt($user_login)) {
            
              $user_type = Auth::user()->user_type;
              $verified = Auth::user()->is_verified;

              if (!empty($verified) || $verified !== 0) {
                return response()->json(['status' => true, 'message' => 'Accept!', 'user_type' => $user_type]);
              }else{
                $validator->errors()->add('email_address', 'Account is not yet verified!');
                return response()->json(['status' => false, 'error' => $validator->errors()]);
              }
            
            }else{
                $validator->errors()->add('email_address', 'Invalid Account!');
                $validator->errors()->add('password', 'Invalid Account!');
                return response()->json(['status' => false, 'error' => $validator->errors()]);
          }
    }

  }

  function logout_admin(){
    auth()->guard('admin')->logout();
    return redirect('/');
  }

  function logout_user(){
      Auth::logout();
      return redirect('/');
  }

  public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('/');
        }
    }
  
    public function register(){
      return view('Account.register');
    }

    function register_account(Request $request){

      $user_id= $request->get('user_id');
      $firstname = $request->get('firstname');
      $lastname = $request->get('lastname');
      $middlename = $request->get('middlename');
      $email_address = $request->get('email_address');
      $password = $request->get('password');
      
      $validator = Validator::make($request->all(), [
        'firstname' => 'required',
        'lastname' => 'required',
        'middlename' => 'required',
        'email_address' => 'required|email',
        'password' => 'required',
        
      ]);
  
      if ($validator->fails()) {
        return response()->json(['status' => false, 'error' => $validator->errors()]);
      }else{
        $employee = new User;
        $employee->firstname = $firstname;
        $employee->lastname = $lastname;
        $employee->middlename = $middlename;
        $employee->email_address = $email_address;
        $employee->password = $password;
        
        if($employee->save()){
          return response()->json(['status' => true, 'message' => 'Employee saved successfully!']);
        }
      }
    }

    function get_email(Request $request){

      $email_address = $request->get('email_address');
      $check_email = User::where('email_address', $email_address)->get();
      $validator = Validator::make($request->all(), [
        'email_address' => 'required|email',
      ]);
      if ($validator->fails()) {
        return response()->json(['status' => '404', 'error' => $validator->errors()]);
      }else{
        if($check_email->count() > 0){
          return response()->json(['status' => '1', 'message' => "Email is valid!"]);
        }else{
          return response()->json(['status' => '0', 'message' => "Email is invalid!"]);
        }
      }
    }

    function load_question(Request $request){
      $email_address = $request->get('email_address');
      $check_email = UserOption::inRandomOrder()->where('email_address',$email_address)->get();
      $row = $check_email->first();

      if($check_email->count() > 0){
        return $row->question;
      }else{
        return '404';
      }

    }

    function submit_answer(Request $request){

      $email_address = $request->get('email_address');
      $question = $request->get('question');
      $answer = $request->get('answer');
      $check_answer =  UserOption::where('email_address', $email_address)->where('question',$question)->get();
      $row = $check_answer->first();

      $validator = Validator::make($request->all(), [
        'email_address' => 'required|email',
      ]);

      if ($validator->fails()) {
        return response()->json(['status' => false, 'error' => $validator->errors()]);
      }else{

        if(strtolower($row->answer) == strtolower($answer)){
          return response()->json(['status' => true, 'message' => 'Your answer is Correct.']);
        }else{
          return response()->json(['status' => false, 'message' => 'Your answer is Incorrect.']);
        }

      }
    }

    function save_new_pass(Request $request){

      $email_address = $request->get('email_address');
      $newpassword = $request->get('newpassword');
      $password_confirmation = $request->get('password_confirmation');

      $validator = Validator::make($request->all(), [
        'newpassword' => 'required',
        'password_confirmation' => 'required'
      ]);

      if ($newpassword != $password_confirmation){
        $validator->errors()->add('password_confirmation', 'Confirm Password does not match with your new password');
        return response()->json(['status' => false, 'error' => $validator->errors()]);
      }
      else{
        
        if (!empty($email_address)) {
            $query =  User::where('email_address', $email_address)->get();
            $row = $query->first();

            $employee = User::find($row->user_id);
            $employee->password = $newpassword;
            if($employee->save()){
                return response()->json(['status' => true, 'message' => 'Password changed successfully']);
            }
        }

      }

    }

}
