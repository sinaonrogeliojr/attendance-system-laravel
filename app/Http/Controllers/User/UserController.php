<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Attendance;
use App\User;
use App\UserOption;
use Auth;
use Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('User.index');
    }

    public function time_log(Request $request){

        $userid = auth()->id();
        $date_today = date('Y-m-d');
        $type =  $request->get('type');
        $check_attendance = Attendance::where('user_id', $userid)->where('date_trans',$date_today)->get();
        $row = $check_attendance->first();
        if($type == 'time_in_f'){
            $lbl = "Time in First";
            $log_status = "You already stored your Time In A.M.";
        }elseif($type == 'time_out_f'){
            $lbl = "Time Out First";
            $log_status = "You already stored your Time Out A.M.";
        }elseif($type == 'time_in_l'){
            $lbl = "Time In Last";
            $log_status = "You already stored your Time In P.M.";
        }elseif($type == 'time_out_l'){
            $lbl = "Time Out Last";
            $log_status = "You already stored your Time Out P.M.";
        }
        $desc = $lbl.' successfully logged!';

        if($check_attendance->count() > 0){
            //update
            
            if($row->$type <> ''){
                //check if already stored data on specific field
                return response()->json(['status' => false, 'message' => $log_status]);
            }else{
                
                $attendance = Attendance::find($row->id);
                $attendance->date_trans =  $date_today;
                $attendance->date_trans =  $date_today;
                $attendance->$type = date('h:i A');
                $attendance->created_at = date('Y-m-d H:i:s');
                if($attendance->save()){
                    return response()->json(['status' => true, 'message' => $desc]);
                }

            }

        }else{
            //insert
            $attendance = new Attendance;
            $attendance->user_id = $userid;
            $attendance->date_trans =  $date_today;
            $attendance->date_trans =  $date_today;
            $attendance->$type = date('h:i A');
            $attendance->created_at = date('Y-m-d H:i:s');
            if($attendance->save()){
                return response()->json(['status' => true, 'message' => $desc]);
            }

        }

    }

    public function time_log_button(Request $request){

        $time_now = date('H:i');
       // $time_now = '17:00;'
        if($time_now >= '08:00' && $time_now <= '11:59'){
            echo 'time_in_f';
        }elseif($time_now >= '12:00' && $time_now <= '12:59'){
            echo 'time_out_f';
        }elseif($time_now >= '13:00' && $time_now <= '16:59'){
            echo 'time_in_l';
        }elseif($time_now >= '17:00'){
            echo 'time_out_l';
        }else{
            echo 'no_time_log';
        }
    }

    public function load_logs_today(){
        $date_today = date('Y-m-d');
        $userid = auth()->id();
        $attendance = Attendance::where('date_trans', $date_today)->where('user_id', $userid)->whereNull('deleted_at')->get();
		return response()->json(['status' => true, 'data' => $attendance]);
    }

    public function accountsettings(){
        return view('User/accountsettings');
    }

    function update_accountsetting(Request $request){

		$user_id= $request->get('user_id');
		$firstname = $request->get('firstname');
		$lastname = $request->get('lastname');
		$middlename = $request->get('middlename');
		$email_address = $request->get('email_address');
        $gender = $request->get('gender');
		$birthday = $request->get('birthday');
		$address = $request->get('address');
		$country = $request->get('country');

		$validator = Validator::make($request->all(), [
			'firstname' => 'required',
			'lastname' => 'required',
			'middlename' => 'required',
			'email_address' => 'required|email',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($user_id)) {
				$employee = User::find($user_id);
				$employee->firstname = $firstname;
				$employee->lastname = $lastname;
				$employee->middlename = $middlename;
				$employee->email_address = $email_address;
                $employee->gender = $gender;
				$employee->birthdate = $birthday;
				$employee->address = $address;
				$employee->country = $country;
				if($employee->save()){
					return response()->json(['status' => true, 'message' => 'Account Settings updated successfully!']);
				}
			}
		}
    }

    function changepassword(Request $request){

        $user_id= $request->get('user_id');
        $currentpassword = $request->get('currentpassword');
        $newpassword = $request->get('newpassword');
        $confirmpassword = $request->get('password_confirmation');

        $validator = Validator::make($request->all(), [
            'currentpassword' => 'required',
            'newpassword' => 'required',
            'password_confirmation' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }
        elseif (!(Hash::check($currentpassword, Auth::user()->password))){
            $validator->errors()->add('currentpassword', 'Your current password does not match with your provided');
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }
        elseif (strcmp($currentpassword,$newpassword)==0){
            $validator->errors()->add('newpassword', 'Your current password cannot be same with the new password');
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }
        elseif ($newpassword != $confirmpassword){
            $validator->errors()->add('password_confirmation', 'Confirm Password does not match with your new password');
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }
        else{
            if (!empty($user_id)) {
                $employee = User::find($user_id);
                $employee->password = $newpassword;
                if($employee->save()){
                    return response()->json(['status' => true, 'message' => 'Password changed successfully']);
                }
            }
        }
    }

    function add_security_option(Request $request){

        $user_id= auth()->id();
		$question = $request->get('question');
        $answer = $request->get('answer');
        $email_address = $request->get('email_address');

        $check_duplicate = UserOption::where('user_id', $user_id)->where('question',$question)->get();
       
        if($check_duplicate->count() > 0){
            return response()->json(['status' => '3', 'message' => 'Question Already exists. Please select other security option.']);
        }else{

            $user_option = new UserOption;
            $user_option->user_id = $user_id;
            $user_option->email_address =  $email_address;
            $user_option->question =  $question;
            $user_option->answer =$answer;
            $user_option->created_at = date('Y-m-d H:i:s');

            if($user_option->save()){
                
                $counter = UserOption::where('user_id', $user_id)->get();
                if($counter->count() > 2){
                    
                    $user_up = User::find($user_id);
                    $user_up->security_options = 1;
                    if($user_up->save()){
                        return response()->json(['status' => '1', 'message' => 'Thank you for you answers.']);
                    }

                }else{
                    return response()->json(['status' => '2', 'message' => 'Successfully saved...']);
                }
                

            }

        }
    
    }

    function question_counter(){
        $user_id= auth()->id();
        $counter = UserOption::where('user_id', $user_id)->get();
        return $counter->count();
    }

}
