<!DOCTYPE html>
<html>
  <!-- Header css meta -->
   @include('Layout.header', ['type' => 'user', 'title' => 'Account Settings', 'icon' => asset('img/logo.png') ])
<body class="sidebar-mini layout-fixed">
  <div class="wrapper">
  <!-- navbar -->
  @include('Layout.nav', ['type' => 'user'])
  <!-- Sidebar -->
  @include('Layout.sidebar', ['type' => 'user'])
    <div class="content-wrapper">
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12 mt-3">
              <div class="card">
                <div class="card-header bg-light">
                  <div class="card-header h4"><i class="fas fa-cog"></i> <span>Account Settings</span></div>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="card">
                        <div class="card-header bg-light">
                          Profile
                        </div>
                          <ul class="list-group list-group-flush">
                            <li class="list-group-item">Name <span class="float-right">{{ Auth::user()->fullname }}</span></li>
                            <li class="list-group-item">Email <span class="float-right">{{ Auth::user()->email_address }}</span></li>
                            <li class="list-group-item">Date Registered <span class="float-right">{{ \Carbon\Carbon::parse(Auth::user()->created_at)->format('M d, Y')}}</span></li>
                          </ul>
                          
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="card">
                        <div class="card-header bg-light">
                          Account Settings
                        </div>
                        <div class="card-body">
                            <form class="needs-validation" id="update_form_id" action="{{ url('/user/update_accountsetting') }}" novalidate>
                                <div class="form-row">
                                    <input type="hidden" id="user_id" name="user_id" placeholder="" value='{{ Auth::user()->user_id }}' class="form-control" required>
                                    <div class="form-group col-sm-12">
                                        <label>First Name </label>
                                        <input type="text" id="firstname" name="firstname" placeholder="First Name" value='{{ Auth::user()->firstname }}' class="form-control " required>
                                        <div class="invalid-feedback" id="err_firstname"></div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label>Middle Name </label>
                                        <input type="text" id="middlename" name="middlename" placeholder="Middle Name" value='{{ Auth::user()->middlename }}' class="form-control " required>
                                        <div class="invalid-feedback" id="err_middlename"></div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label>Last Name </label>
                                        <input type="text" id="lastname" name="lastname" placeholder="Last Name" value='{{ Auth::user()->lastname }}' class="form-control " required>
                                        <div class="invalid-feedback" id="err_lastname"></div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label>Email </label>
                                        <input type="email" id="email_address" name="email_address" placeholder="Email Address" value='{{ Auth::user()->email_address }}' class="form-control " required>
                                        <div class="invalid-feedback" id="err_email_address"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                          <div class="form-group col-sm-6">
                                            <label>Gender </label>
                                            <select class="form-control" id="gender" name="gender">
                                                <option value="Female"  @if(Auth::user()->gender == "Female") selected @endif>Female</option>
                                                <option value="Male" @if(Auth::user()->gender == "Male") selected @endif>Male</option>
                                            </select>
                                            <div class="invalid-feedback" id="err_gender"></div>
                                          </div>
                                          <div class="form-group col-sm-6">
                                            <label>Birthday </label>
                                            <input type="date" id="birthday" value="{{ Auth::user()->birthdate }}" name="birthday" placeholder="Birthday" class="form-control " required>
                                            <div class="invalid-feedback" id="err_birthday"></div>
                                          </div>
                                          
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="row">
                                          <div class="form-group col-sm-6">
                                            <label>Address </label>
                                            <input type="text" id="address" value="{{ Auth::user()->address }}" name="address" placeholder="Address" class="form-control " required>
                                            <div class="invalid-feedback" id="err_address"></div>
                                          </div>
                                          <div class="form-group col-sm-6">
                                            <label>Country </label>
                                            <input type="text" id="country" value ="{{ Auth::user()->country }}" name="country" placeholder="Country" class="form-control " required>
                                            <div class="invalid-feedback" id="err_country"></div>
                                          </div>
                                          
                                        </div>
                                      </div>
                                    <div class="col-sm-12 text-right">
                                        <button class="btn btn-secondary" type="submit">Save</button>
                                        <button type="button" class="btn btn-outline-secondary" onclick="change_password()">Change Password</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-footer"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>

    <!-- Modal Change Password -->
    <div class="modal fade" role="dialog" id="modal_change_password">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Change Password
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" id="changepassword_id" action="{{ url('/user/changepassword') }}" novalidate>
              <div class="form-row">
                <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->user_id }}" placeholder="" class="form-control" required>
                <div class="form-group col-sm-12">
                  <label>Current Password </label>
                  <input type="password" id="currentpassword" name="currentpassword" placeholder="Current Password" class="form-control " required>
                  <div class="invalid-feedback" id="err_currentpassword"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>New Password </label>
                  <input type="password" id="newpassword" name="newpassword" placeholder="New Password" class="form-control " required>
                  <div class="invalid-feedback" id="err_newpassword"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Confirm Password </label>
                  <input type="password" id="password_confirmation" name="password_confirmation" placeholder="Confirm New Password" class="form-control " required>
                  <div class="invalid-feedback" id="err_password_confirmation"></div>
                </div>

                <div class="col-sm-12 text-right">
                  <button class="btn btn-secondary" type="submit">Save</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>



  </div>
</body>
  <!-- Footer Scripts -->
  @include('Layout.footer', ['type' => 'user'])
</html>

<script>

$("#update_form_id").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'update_form_id');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'update_form_id');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

  // Modal Change Password

  function change_password(){
    $("#modal_change_password").modal('show');
  }

  $("#changepassword_id").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'changepassword_id');
          $('#modal_change_password').modal('hide');
          $('body').removeClass('modal-open');
          $('.modal-backdrop').remove();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'changepassword_id');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});



</script>
