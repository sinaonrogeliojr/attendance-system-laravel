@php
  $parent = '';
  $child = '';
  $active = url()->current();
@endphp

@if($type == 'home')

@elseif($type == 'user')
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{ url('user') }}" class="brand-link">
    <img src="{{ asset('img/logo.png') }}"
         alt="AdminLTE Logo"
         class="brand-image img-circle elevation-3 bg-white">
         <span class="brand-text font-weight-light">User Account</span>
  </a>
  <!-- Sidebar -->
  <div class="sidebar" >
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        @if(!empty(Auth::user()->profile_picture))
          <img src="{{ asset('profile_picture/'.Auth::user()->profile_picture) }}" class="img-circle elevation-2" alt="User Image">
        @else
          <img src="{{ asset('img/avatar.png') }}" class="img-circle elevation-2" alt="User Image">
        @endif
      </div>
      <div class="info">
        <a href="#" class="d-block">{{ Auth::user()->fullname }}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview active">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
@elseif($type == 'admin')
 <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('admin') }}" class="brand-link">
      <img src="{{ asset('img/logo.png') }}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3 bg-white">
           <span class="brand-text font-weight-light">Admin</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar" >
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          @if(!empty(Auth::user()->profile_picture))
            <img src="{{ asset('profile_picture/'.Auth::user()->profile_picture) }}" class="img-circle elevation-2" alt="User Image">
          @else
            <img src="{{ asset('img/avatar.png') }}" class="img-circle elevation-2" alt="User Image">
          @endif
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->fullname }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ url('admin') }}" class="nav-link {{ (request()->is('admin')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-plus fa-lg"></i>
              <p>
                User Acounts
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('attendance') }}" class="nav-link {{ (request()->is('attendance')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-clock fa-lg"></i>
              <p>
                Attendance
              </p>
            </a>
          </li>
           <!-- <li class="nav-header">EXAMPLES</li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
@endif
