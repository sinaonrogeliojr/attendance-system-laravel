<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::namespace('Account')->group(function () {
	Route::get('/', function () {
	    return view('Account.index');
	});

	Route::get('/register', function () {
	    return view('Account.register');
	});

	Route::post('/check_user', 'AccountController@login_user');
	Route::post('/register_account', 'AccountController@register_account');

	Route::get('/logout/admin','AccountController@logout_admin')->name('logout.admin');

	Route::get('/logout/user','AccountController@logout_user')->name('logout.user');

	Route::get('/f_password', function(){
	    return view('Account.forgot_password');
	});

	Route::post('/get_email', 'AccountController@get_email');
	Route::post('/load_question', 'AccountController@load_question');
	Route::post('/submit_answer', 'AccountController@submit_answer');
	Route::post('/save_new_pass', 'AccountController@save_new_pass');

});

Route::namespace('Admin')->group(function () {
	Route::middleware(['auth'])->group(function () {
        
		Route::get('/system/settings', 'AdminController@settings')->name('admin.settings');

		Route::get('/admin', 'AdminController@index')->name('admin.index');
		Route::get('/attendance', 'AdminController@attendance')->name('admin.attendance');
		Route::get('/admin/list_attendance/{date_from}/{date_to}', 'AdminController@list_attendance')->name('admin.attendance');
		
		// Crud
		Route::get('/admin/list_employee', 'AdminController@list_employee')->name('employee.list');
		Route::post('/admin/add_employee', 'AdminController@add_employee')->name('employee.add');
		Route::get('/admin/delete_employee/{user_id}', 'adminController@delete_employee')->name('employee.delete');
		
	});
});

Route::namespace('User')->group(function () {
	Route::middleware(['auth'])->group(function () {
		//Route::get('/user', 'UserController@index')->name('user.index');
		Route::get('/user', 'UserController@index');
		Route::post('/user/time_log', 'UserController@time_log')->name('user.time_log');
		Route::post('/user/time_log_button', 'UserController@time_log_button')->name('user.time_log_button');
		Route::get('/user/load_logs_today', 'UserController@load_logs_today')->name('user.attendance');

		Route::get('/user/accountsettings', 'UserController@accountsettings')->name('user.accountsettings');
		Route::post('/user/update_accountsetting', 'UserController@update_accountsetting')->name('user.update_accountsetting');
		// Change Password
		Route::post('/user/changepassword', 'UserController@changepassword')->name('user.changepassword');
		Route::post('/user/add_security_option', 'UserController@add_security_option')->name('employee.add_security_option');
		Route::get('/user/question_counter', 'UserController@question_counter')->name('user.question_counter');
	});
});